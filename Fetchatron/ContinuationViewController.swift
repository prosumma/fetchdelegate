//
//  ContinuationViewController.swift
//  Fetchatron
//
//  Created by Gregory Higley on 11/13/15.
//  Copyright © 2015 Gregory Higley. All rights reserved.
//

import UIKit

class ContinuationViewController: ImageViewController {
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        fetcher.fetch() { image in
            guard let image = image else {
                // We failed to get the image
                return
            }
            self.imageView.image = image
        }
    }
    
}
