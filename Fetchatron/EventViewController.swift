//
//  EventViewController.swift
//  Fetchatron
//
//  Created by Gregory Higley on 11/13/15.
//  Copyright © 2015 Gregory Higley. All rights reserved.
//

import UIKit

class EventViewController: ImageViewController {
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        fetcher.fetched += fetcherDidFetch
        fetcher.fetch()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    private func fetcherDidFetch(sender: AnyObject?, args: Fetcher.EventArgs) {
        guard let image = args.image else {
            return
        }
        imageView.image = image
    }
    
}