//
//  NotificationViewController.swift
//  Fetchatron
//
//  Created by Gregory Higley on 11/13/15.
//  Copyright © 2015 Gregory Higley. All rights reserved.
//

import UIKit

class NotificationViewController: ImageViewController {
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "fetcherDidFetchImage:", name: Fetcher.DidFetchImageNotification, object: nil)
        fetcher.fetch()
    }
    
    override func viewDidDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        super.viewDidDisappear(animated)
    }

    func fetcherDidFetchImage(notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        guard let image = userInfo["image"] as? UIImage else {
            return
        }
        imageView.image = image
    }
    
}
