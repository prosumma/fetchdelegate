//
//  Fetch.swift
//  Fetchatron
//
//  Created by Gregory Higley on 11/13/15.
//  Copyright © 2015 Gregory Higley. All rights reserved.
//

import UIKit

protocol FetchDelegate: class {
    func fetcher(fetcher: Fetcher, didFetchImage image: UIImage?)
}

/**
 The `Fetcher` class fetches a specific image and displays it.
 It supports two styles of asynchronous fetching: the delegate
 pattern and the continuation-passing pattern.
*/
class Fetcher {
    
    static let DidFetchImageNotification = "FetcherDidFetchImageNotification"
    
    private static let IMAGE_URL = NSURL(string: "https://s3.amazonaws.com/revolucent.net/anarchists/MurrayBW.jpg")!
    
    /**
     - warning: If nil is passed, the image was not fetched.
    */
    typealias FetchCallback = UIImage? -> Void
    
    weak var delegate: FetchDelegate?
    
    class EventArgs: Fetchatron.EventArgs {
        let image: UIImage?
        
        init(image: UIImage?) {
            self.image = image
        }
    }
    
    let fetched = Event<EventArgs>()
    
    func fetch(callback: FetchCallback? = nil) {
        let task = NSURLSession.sharedSession().dataTaskWithURL(Fetcher.IMAGE_URL) { (data, response, error) in
            // NSURLSession runs this code on a background thread. Do not talk to the UI here.
            guard let data = data else {
                print(error)
                print(response)
                return
            }
            let image = UIImage(data: data)
            // All of our events happen on the main thread.
            dispatch_async(dispatch_get_main_queue()) {
                // Here we call our callback
                callback?(image)
                // Here we call a method on our delegate
                self.delegate?.fetcher(self, didFetchImage: image)
                // Here we post a notification
                var userInfo = [NSObject: AnyObject]()
                if let image = image {
                    userInfo["image"] = image
                }
                let notification = NSNotification(name: Fetcher.DidFetchImageNotification, object: self, userInfo: userInfo)
                NSNotificationCenter.defaultCenter().postNotification(notification)
                // C#-style event
                self.fetched.fire(self, args: EventArgs(image: image))
            }
        }
        task.resume()
    }
    
}

