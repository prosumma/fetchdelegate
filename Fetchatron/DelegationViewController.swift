//
//  DelegationViewController.swift
//  Fetchatron
//
//  Created by Gregory Higley on 11/13/15.
//  Copyright © 2015 Gregory Higley. All rights reserved.
//

import UIKit

class DelegationViewController: ImageViewController, FetchDelegate {
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        fetcher.delegate = self
        fetcher.fetch()
    }

    func fetcher(fetcher: Fetcher, didFetchImage image: UIImage?) {
        guard let image = image else {
            // We failed to get the image
            return
        }
        imageView.image = image
    }
    
}
