//
//  BaseViewController.swift
//  Fetchatron
//
//  Created by Gregory Higley on 11/13/15.
//  Copyright © 2015 Gregory Higley. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    let fetcher = Fetcher()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red: 0.11, green: 0.11, blue: 0.10, alpha: 1.0)
        navigationItem.title = "Murray Rothbard"
    }
    
}