//
//  Event.swift
//  Fetchatron
//
//  Created by Gregory Higley on 11/13/15.
//  Copyright © 2015 Gregory Higley. All rights reserved.
//

import Foundation

/**
 Base class for EventArgs
 - note: See Fetcher.EventArgs class for an example.
*/
class EventArgs {
    
}

final class Event<E: EventArgs> {
    typealias Handler = (AnyObject?, E) -> Void
    
    private var handlers = [NSUUID: Handler]()
    
    func fire(sender: AnyObject, args: E) {
        for (_, handler) in handlers {
            handler(sender, args)
        }
    }
}

func +=<E: EventArgs>(event: Event<E>, handler: Event<E>.Handler) -> NSUUID {
    let handlerToken = NSUUID()
    event.handlers[handlerToken] = handler
    return handlerToken
}

func -=<E: EventArgs>(event: Event<E>, handlerToken: NSUUID) -> Bool {
    return event.handlers.removeValueForKey(handlerToken) != nil
}

